public with sharing class TestsController {
    @AuraEnabled(cacheable=true)
    public static List<Test__c> getTestList(String contactId) {
        List<Test__c> retList = new List<Test__c>();
        retList = [SELECT Id, Contact__c, Result__c, Test_brand__c, Test_date__c, Test_for__c, Test_type__c FROM Test__c WHERE Contact__c = :contactId ORDER BY Test_date__c DESC ];
        return retList;
    }
}
