public with sharing class SymptomController {
    
    @AuraEnabled(cacheable=false)
    public static Daily_Symptom__c getTodaysSymptom(String contactId) {
        // return todays symptom record if one exists
        // oterhwise return a new symptom record with todays date prefilled
        /*
        Daily_Symptom__c retDaily = null;
        List<Daily_Symptom__c> todaysSymptom = [SELECT Id, Contact__c, Name, Temperature_morning__c, Temperature_day__c, Temperature_evening__c, Reporting_date__c 
          FROM Daily_Symptom__c WHERE Contact__c = :contactId AND
          Reporting_date__c = TODAY];
        if(todaysSymptom.size() == 1 ) {
            // a report already exists for today
            retDaily = todaysSymptom[0];
        } else if (todaysSymptom.size() == 0 ) {
            // no report exists for today
            retDaily = new Daily_Symptom__c();
            retDaily.Contact__c = contactId;
            retDaily.Reporting_date__c = Date.today();
            retDaily.Temperature_morning__c = null;
            retDaily.Temperature_day__c = null;
            retDaily.Temperature_evening__c = null;

        } else {
            // more than 1 report exists. This is an error condition to handle
            system.debug('ERROR Symptomcontroller found more than 1 daily report');
        }
    return retDaily;
    */
    return getDateSymptom(contactId, Date.today());

    }

    @AuraEnabled(cacheable=false)
    public static Daily_Symptom__c getDateSymptom(String contactId, Date dt) {
        // return selecte date symptom record if one exists
        // oterhwise return a new symptom record with selected  date prefilled
        Daily_Symptom__c retDaily = null;
        List<Daily_Symptom__c> daySymptom = [SELECT Id, Contact__c, Name, Temperature_morning__c, Temperature_day__c, Temperature_evening__c, Reporting_date__c, Cough__c,
          Breath_problems__c, Chest_pain__c, Smell_and_taste__c, Infected_contact__c, Visited_high_risk_place__c,  Name_of_visited_place__c, Other_symptoms__c, No_symptoms__c 
          FROM Daily_Symptom__c WHERE Contact__c = :contactId AND
          Reporting_date__c = :dt];
        if(daySymptom.size() == 1 ) {
            // a report already exists for today
            retDaily = daySymptom[0];
        } else if (daySymptom.size() == 0 ) {
            // no report exists for today
            retDaily = new Daily_Symptom__c();
            retDaily.Contact__c = contactId;
            retDaily.Reporting_date__c = dt;
            retDaily.Temperature_morning__c = null;
            retDaily.Temperature_day__c = null;
            retDaily.Temperature_evening__c = null;

        } else {
            // more than 1 report exists. This is an error condition to handle
            system.debug('ERROR Symptomcontroller found more than 1 daily report');
        }
    return retDaily;

    }




    @AuraEnabled(cacheable=true)
    public static Date lastDay() {
        // return date of last day to forward to - today
        //Boolean retLast = true;
        //If(dt<Date.today()) {
        //    retLast = false;
        //}
        return date.Today();
    }

    @AuraEnabled(cacheable=false)
    public static Daily_Symptom__c saveSymptom(Daily_Symptom__c ds) {
        upsert ds;
        return ds;
    }
}
