public with sharing class CommunityContactController {
    @AuraEnabled(cacheable=true)
    public static String getContactId() {
        // return the ContactId of the logged in community user if running in a community context
        // otherwise return null
        String retId = null;
        List<User>  usr =[SELECT Id, AccountId, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        if(usr.size() == 1 && usr[0].AccountId != null) {  // AccountId only has a value for community users
            retId = usr[0].ContactId;
        } 
        return retId;
    }

    @AuraEnabled(cacheable=true)
    public static Contact getContact(String contactId) {
        // return the Contact or null if no uniqque match
        Contact  retContact = null;
        List<Contact>  contacts =[SELECT Id, FirstName, LastName FROM Contact WHERE Id = :contactId];
        if(contacts.size() == 1 ) {  
            retContact = contacts[0];
        } 
        return retContact;
    }


}
