import { LightningElement, api, wire, track } from 'lwc';
import getContactId from '@salesforce/apex/CommunityContactController.getContactId';
import getTestList from '@salesforce/apex/TestsController.getTestList';
import { refreshApex } from '@salesforce/apex';
import { deleteRecord } from 'lightning/uiRecordApi';

export default class TestList extends LightningElement {

error;
    communityContactId;
    contactId;
    testId;
    list = true;
    @track testList;

    @api recordId;
    
    // need to otpimize loading to eliminate extra server roundtrips

    @wire(getContactId) 
    wiredContactId ({error, data}) {
        //console.log('contactId in');
        if (error) {
            //console.log('contactId fail');
            this.communityContactId = undefined;
            this.error = error;
            //console.log(error);
        
        } else  {
            //console.log('contactId success');
            if (data) {
                this.communityContactId = data;
                this.contactId = data;
                //console.log(data);
                this.error = undefined;
            } else {
                this.communityContactId = null;
                this.contactId = this.recordId;

                //console.log(data);
                this.error = undefined;
            }
            
        }
    };

    @wire(getTestList, { contactId: '$contactId' })
    testList;

    @api 
    get edit() {
        return !this.list;
    }

    handleSave() {
        console.log('save');
        this.list = true
    }

    handleSucess(event){
        const updatedRecord = event.detail.id;
        console.log('onsuccess: ', updatedRecord);
        this.testId = null;
        this.list = true;
        refreshApex(this.testList);
        
        
     }

    handleCancel() {
        console.log('cancel');
        this.list = true
    }

    handleNew() {
        console.log('New');
        this.list = false;
    }

    handleEdit(event) {
        console.log('Edit');
        const id = event.target.dataset.name;
        console.log(id);
        this.testId = id;
        this.list = false;

    }

    handleDelete(event) {
        console.log('Delete');
        const id = event.target.dataset.name;
        console.log(id);
        deleteRecord(id)
            .then(() => {
                console.log('delete success');
                refreshApex(this.testList);
                this.list = true;
            })
            .catch(error => {
                console.log('delete fail');
                console.log(error);
            });
    }
}