import { LightningElement, api, wire, track } from 'lwc';
import getTodaysSymptom from '@salesforce/apex/SymptomController.getTodaysSymptom';
import getDateSymptom from '@salesforce/apex/SymptomController.getDateSymptom';
import lastDay from '@salesforce/apex/SymptomController.lastDay';
import saveSymptom from '@salesforce/apex/SymptomController.saveSymptom';

export default class DailySymptom extends LightningElement {
    @api contactId;

    @track symptom;
    changed = false;
    error;
    initialized = false;
    last;
    symptomBackup;
    reportDate;

   

    @wire(lastDay) 
    wiredlastDay ( {error, data} ) {
        console.log('last day in');
        //if(this.symptom !== undefined) {
        Y//    console.log(this.symptom.Reporting_date__c);
            if (data) {
                console.log('last success');
                this.last = data;
                console.log(data);
                this.error = undefined;
            } else if (error) {
                console.log('last error');
                this.last = undefined;
                this.error = error;
                console.log(error);
            }
        //}
    };


    
   renderedCallback() {
    if (this.initialized) {
        return;
    }
    this.initialized = true;
    console.log('callback');
    getTodaysSymptom({ contactId: this.contactId })
        .then(result => {
            console.log('load success');
            this.symptom = result;
            this.symptomBackup = JSON.parse(JSON.stringify(result));
            let rDate = new Date(Date.parse(result.Reporting_date__c));
            console.log(rDate);
            console.log(rDate.toISOString().split('T')[0]);
            this.reportDate = rDate.toISOString().split('T')[0];
        })
        .catch(error => {
            console.log('load fail');
            console.log(error);
            this.error = error;
        });
    }


    handleTempChange(event) {
        const slider = event.target.dataset.name;
        console.log(slider);
        this.symptom.No_symptoms__c = false;
        this.changed = true;
        switch (slider) {
            case 'morning':
                this.symptom.Temperature_morning__c = event.target.value;
                break;
            case 'day':
                this.symptom.Temperature_day__c = event.target.value;
                break;
            case 'evening':
                this.symptom.Temperature_evening__c = event.target.value;
                break;
            default:
                console.log('illegal slider name');

        }
        //console.log(event.target.value);
        //console.log(this.symptom.Temperature_morning__c);
        //console.log(JSON.parse(JSON.stringify(this.symptom)));
        
        //this.symptom.Temperature_morning__c = 38;
    }

    handleSympChange(event) {
        const slider = event.target.dataset.name;
        //console.log(slider);
        this.symptom.No_symptoms__c = false;
        this.changed = true;
        switch (slider) {
            case 'cough':
                this.symptom.Cough__c = event.target.value;
                break;
            case 'breath':
                this.symptom.Breath_problems__c = event.target.value;
                break;
            case 'chest':
                this.symptom.Chest_pain__c = event.target.value;
                break;
            case 'smell':
                this.symptom.Smell_and_taste__c = event.target.value;
                break;    
            default:
                console.log('illegal slider name');

        }
        //console.log(event.target.value);
        //console.log(this.symptom.Temperature_morning__c);
        //console.log(JSON.parse(JSON.stringify(this.symptom)));
        
        //this.symptom.Temperature_morning__c = 38;
    }

    handleCheckboxChange(event) {
        //console.log('checkbox');
        this.changed = true;
        const cb = event.target.dataset.name;
        //console.log(cb);
        switch (cb) {
            case 'nosymtoms':
                //console.log(this.symptom.Infected_contact__c);
                if(this.symptom.No_symptoms__c == undefined || this.symptom.No_symptoms__c == false) {
                    this.symptom.No_symptoms__c = true;
                    this.symptom.Temperature_morning__c = null;
                    this.symptom.Temperature_day__c = null;
                    this.symptom.Temperature_evening__c = null;
                    this.symptom.Cough__c = null;
                    this.symptom.Breath_problems__c = null;
                    this.symptom.Chest_pain__c = null;
                    this.symptom.Smell_and_taste__c = null;
                }  else {
                    this.symptom.No_symptoms__c = false;
                }
                //console.log(this.symptom.Infected_contact__c);
                break;
            case 'infected':
                //console.log(this.symptom.Infected_contact__c);
                if(this.symptom.Infected_contact__c == undefined || this.symptom.Infected_contact__c == false) {
                    this.symptom.Infected_contact__c = true;
                }  else {
                    this.symptom.Infected_contact__c = false;
                }
                //console.log(this.symptom.Infected_contact__c);
                break;
            case 'visited':
                //console.log(this.symptom.Visited_high_risk_place__c);
                if(this.symptom.Visited_high_risk_place__c == undefined || this.symptom.Visited_high_risk_place__c == false) {
                    this.symptom.Visited_high_risk_place__c = true;
                }  else {
                    this.symptom.Visited_high_risk_place__c = false;
                }
                //console.log(this.symptom.Visited_high_risk_place__c);
                break;   
            default:
                console.log('illegal slider name');

        }


        console.log(event.value);
    }

    handlePlaceChange(event) {
        console.log('place');
        console.log(event.detail.value);
        this.symptom.Name_of_visited_place__c = event.detail.value;
        this.changed = true;
    }

    handleOtherChange(event) {
        //console.log('other');
        // console.log(event.detail.value);
        this.symptom.Other_symptoms__c = event.detail.value;
        this.changed = true;
    }

    @api 
    get saveDisabled() {
        return !this.changed;
    }

    @api 
    get fwdDisabled() {
        
       console.log('fwd disabled');
       console.log(this.last);
       let lastDate = new Date(Date.parse(this.last));
       let currentDate = new Date(Date.parse(this.reportDate));

        return currentDate >= lastDate || this.changed;
    }

    @api 
    get prevDisabled() {
        
       console.log('prev disabled');
        return this.changed;
    }

    handleCancel() {
        console.log('cancel');
        console.log(JSON.stringify(this.symptom));
        console.log(JSON.stringify(this.symptomBackup));
        this.symptom = JSON.parse(JSON.stringify(this.symptomBackup));
        this.changed = false;
    }

    handleSave() {
        console.log('save');
        saveSymptom({ ds: this.symptom })
        .then(result => {
            console.log('save success');
            this.symptom = result;
            this.symptomBackup = JSON.parse(JSON.stringify(result));
            this.changed = false;
        })
        .catch(error => {
            console.log('save fail');
            console.log(error);
            this.error = error;
        });
       
    }

    handlePrev() {
        console.log('prev');
        let prevDate = new Date(Date.parse(this.reportDate));
        prevDate.setDate(prevDate.getDate()-1);
        console.log(prevDate.toISOString().split('T')[0]);
        getDateSymptom({ contactId: this.contactId, dt: prevDate })
        .then(result => {
            console.log('load success');
            this.symptom = result;
            this.symptomBackup = JSON.parse(JSON.stringify(result));
            let rDate = new Date(Date.parse(result.Reporting_date__c));
            console.log(rDate);
            console.log(rDate.toISOString().split('T')[0]);
            this.reportDate = rDate.toISOString().split('T')[0];
            this.changed = false;
        })
        .catch(error => {
            console.log('load fail');
            console.log(error);
            this.error = error;
        });
    }

    handleNext() {
        console.log('next');
        let nextDate = new Date(Date.parse(this.reportDate));
        nextDate.setDate(nextDate.getDate()+1);
        console.log(nextDate.toISOString().split('T')[0]);
        getDateSymptom({ contactId: this.contactId, dt: nextDate })
        .then(result => {
            console.log('load success');
            this.symptom = result;
            this.symptomBackup = JSON.parse(JSON.stringify(result));
            let rDate = new Date(Date.parse(result.Reporting_date__c));
            console.log(rDate);
            console.log(rDate.toISOString().split('T')[0]);
            this.reportDate = rDate.toISOString().split('T')[0];
            this.changed = false;
        })
        .catch(error => {
            console.log('load fail');
            console.log(error);
            this.error = error;
        });
       
    }

    handleToday() {
        console.log('today');
        let todayDate = new Date(Date.parse(this.last));
        console.log(todayDate.toISOString().split('T')[0]);
        getDateSymptom({ contactId: this.contactId, dt: todayDate })
        .then(result => {
            console.log('load success');
            this.symptom = result;
            this.symptomBackup = JSON.parse(JSON.stringify(result));
            let rDate = new Date(Date.parse(result.Reporting_date__c));
            console.log(rDate);
            console.log(rDate.toISOString().split('T')[0]);
            this.reportDate = rDate.toISOString().split('T')[0];
            this.changed = false;
        })
        .catch(error => {
            console.log('load fail');
            console.log(error);
            this.error = error;
        });
       
    }

}