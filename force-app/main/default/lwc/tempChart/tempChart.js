import { LightningElement, api, track } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
// import chartjs from '@salesforce/resourceUrl/Chart_js';
import chartjs from '@salesforce/resourceUrl/chartjs';

const generateRandomNumber = () => {
    return Math.round(Math.random() * 100);
};

export default class TempChart extends LightningElement {
    error;
    @api chart;
    @track chartjsInitialized = false;

    config = {
        type: 'doughnut',
        data: {
            datasets: [
                {
                    data: [
                        generateRandomNumber(),
                        generateRandomNumber(),
                        generateRandomNumber(),
                        generateRandomNumber(),
                        generateRandomNumber()
                    ],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 159, 64)',
                        'rgb(255, 205, 86)',
                        'rgb(75, 192, 192)',
                        'rgb(54, 162, 235)'
                    ],
                    label: 'Dataset 1'
                }
            ],
            labels: ['Red', 'Orange', 'Yellow', 'Green', 'Blue']
        },
        options: {
            responsive: true,
            legend: {
                position: 'right'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };


    renderedCallback() {
        if (this.chartjsInitialized) {
            return;
        }
        this.chartjsInitialized = true;

        // loadScript(this, chartjs + '/Chart.js-2.9.3/dist/Chart.js')
        // loadScript(this, chartjs + '/Chart.js-2.9.3/dist/Chart.bundle.min.js')
        loadScript(this, chartjs)
            .then(() => {
                console.log ('chart loaded');
                const canvas = document.createElement('canvas');
                this.template.querySelector('div.chart').appendChild(canvas);
                const ctx = canvas.getContext('2d');

                // var ctx = this.template.querySelector('.linechart').getContext('2d');
                //var ctx = this.template.querySelector('.linechart');
                // const ctx = this.template.querySelector("canvas.tempchart");
                console.log(typeof ctx);
                console.log(ctx);
                console.log(this.config);
                // this.chart = new Chart(ctx, this.config);
                
                // this.chart = new Chart(ctx, this.config);
                
                //this.chart.canvas.parentNode.style.height = '100%';
                //this.chart.canvas.parentNode.style.width = '100%';

                var myChart = new window.Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                        datasets: [{
                            label: '# of Votes',
                            data: [12, 19, 3, 5, 2, 3],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });


                console.log('chart ready');
            })
            .catch(error => {
                console.log('chart error');
                console.log(error);
                this.error = error;
            });
    }


}