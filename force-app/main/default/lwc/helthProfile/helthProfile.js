import { LightningElement, api, wire } from 'lwc';
import getContactId from '@salesforce/apex/CommunityContactController.getContactId';

export default class HelthProfile extends LightningElement {

    error;
    communityContactId;
    contactId;

    @api recordId;
    
    // need to otpimize loading to eliminate extra server roundtrips

    @wire(getContactId) 
    wiredContactId ({error, data}) {
        //console.log('contactId in');
        if (error) {
            //console.log('contactId fail');
            this.communityContactId = undefined;
            this.error = error;
            //console.log(error);
        
        } else  {
            //console.log('contactId success');
            if (data) {
                this.communityContactId = data;
                this.contactId = data;
                //console.log(data);
                this.error = undefined;
            } else {
                this.communityContactId = null;
                this.contactId = this.recordId;

                //console.log(data);
                this.error = undefined;
            }
            
        }
    };
}