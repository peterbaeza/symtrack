import { LightningElement, api, wire } from 'lwc';
//import USER_ID from "@salesforce/user/Id";
import getContactId from '@salesforce/apex/CommunityContactController.getContactId';
import getContact from '@salesforce/apex/CommunityContactController.getContact';


export default class DailyReport extends LightningElement {
    contact;
    error;
    communityContactId;
    contactId;

    @api recordId;
    // userId = USER_ID;
    
    // need to otpimize loading to eliminate extra server roundtrips

    @wire(getContactId) 
    wiredContactId ({error, data}) {
        //console.log('contactId in');
        if (error) {
            //console.log('contactId fail');
            this.communityContactId = undefined;
            this.error = error;
            //console.log(error);
        
        } else  {
            //console.log('contactId success');
            if (data) {
                this.communityContactId = data;
                this.contactId = data;
                //console.log(data);
                this.error = undefined;
            } else {
                this.communityContactId = null;
                this.contactId = this.recordId;

                //console.log(data);
                this.error = undefined;
            }
            
        }
    };
    // contactId = this.communityContactId.data;
    
    

//    @api 
//    get contactId() {
//        return this.communityContactId.data !== null ? this.communityContactId.data : this.recordId;
//    }


    // @wire(getContact, { contactId: '0033N000009kGmcQAE' }) 
    @wire(getContact, { contactId: '$contactId' }) 
    wiredContact ( {error, data} ) {
        //console.log('contact in');
        if (data) {
            //console.log('contact success');
            this.contact = data;
            //console.log(data);
            this.error = undefined;
        } else if (error) {
            //console.log('contact fail');
            this.contact = undefined;
            this.error = error;
            //console.log(error);
        }
    };

}